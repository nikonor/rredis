package rredis

import (
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
)

func TestHealthChecker(t *testing.T) {
	begin()
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	c.DebugToggle()
	dCh := make(chan struct{})
	
	c.StartHealthChecker(time.Second, dCh)
	
	time.Sleep(5 * time.Second)
	close(dCh)
	time.Sleep(time.Second)
}
