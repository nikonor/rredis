package rredis

import (
	"bytes"
	"fmt"
	"sync"
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

var (
	cfg = CfgRedis{
		Host:     "localhost",
		Port:     6379,
		Password: "",
		DB:       0,
	}
	l          sync.Mutex
	isBegining bool
)

func begin() {
	l.Lock()
	defer l.Unlock()
	if !isBegining {
		ll.Start(7)
		ll.StartStdoutStream()
		isBegining = true
	}
}
func beginWLevel(lvl int) {
	l.Lock()
	defer l.Unlock()
	if !isBegining {
		ll.Start(lvl)
		ll.StartStdoutStream()
		isBegining = true
	}
}

func TestRCli_Get(t *testing.T) {
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	i, err := c.GetInt64(logPrefix, "qwe")
	println("#0::i=", i)
	if err == nil || (err != nil && err != ErrNil) {
		t.Error("Ошибка Get #0")
	}
	
	// шаг 1: выставили Int64 и получили Int64 в разных видах
	aa := 123
	c.Set(logPrefix, "qwe", aa, time.Second)
	i, err = c.GetInt64(logPrefix, "qwe")
	if err != nil || i != int64(aa) {
		t.Error("Ошибка Get #1.1")
	}
	s, err := c.GetString(logPrefix, "qwe")
	if err != nil || s != "123" {
		t.Error("Ошибка Get #1.2")
	}
	
	b, err := c.GetBytes(logPrefix, "qwe")
	if err != nil || bytes.Compare(b, []byte(`123`)) != 0 {
		t.Error("Ошибка GetBytes")
	}
	
	f, err := c.GetFloat64(logPrefix, "qwe")
	if err != nil || f != float64(123) {
		t.Error("Ошибка Get #1.3")
	}
	
	// шаг 2: выставили Float64 и получили Float64 в разных видах
	bb := float64(123.45)
	c.Set(logPrefix, "qwe", bb, time.Second)
	i, err = c.GetInt64(logPrefix, "qwe")
	if err == nil {
		t.Error("Ошибка Get #2.1")
	}
	s, err = c.GetString(logPrefix, "qwe")
	if err != nil || s != "123.45" {
		t.Error("Ошибка Get #2.2")
	}
	f, err = c.GetFloat64(logPrefix, "qwe")
	if err != nil || f != float64(bb) {
		t.Error("Ошибка Get #2.3")
	}
	
}

//
// func TestRCli_Incr(t *testing.T) {
//     begin()
//     logPrefix := ee.NewPrefix()
// 	c := Init(logPrefix,cfg)
//
// 	a := int64(0)
// 	c.Set(logPrefix, "qwe", a, time.Second)
//
// 	for i := 0; i < 10; i++ {
// 		got2, err := c.Incr("qwe")
// 		// println(i+1, "got2=", got2)
// 		if err != nil || got2 != int64(i+1) {
// 			t.Errorf("Шаг %d:  Ждали %d, получили %d", int(i/2), i+1, got2)
// 		}
// 	}
// }
//
func TestClient_IncrN(t *testing.T) {
	key := "qwe"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	err := c.Set(logPrefix, key, 0, time.Second)
	if err != nil {
		t.Error(err.Error())
		return
	}
	
	got, err := c.IncrN(logPrefix, key, 2)
	if err != nil || got != int64(2) {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 1, 2, got)
	}
	
	got, err = c.IncrN(logPrefix, key, 2)
	if err != nil || got != int64(4) {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 2, 4, got)
	}
}

func TestClient_Keys(t *testing.T) {
	key1 := "qwe_key1"
	key2 := "qwe_key2"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	var (
		got []string
		err error
	)
	
	err = c.Set(logPrefix, key1, 0, 2*time.Second)
	if err != nil {
		t.Error(err.Error())
		return
	}
	
	got, err = c.Keys(logPrefix, "qwe_*")
	if err != nil || len(got) != 1 {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 1, 1, len(got))
	}
	
	err = c.Set(logPrefix, key2, 0, time.Second)
	if err != nil {
		t.Error(err.Error())
		return
	}
	
	got, err = c.Keys(logPrefix, "qwe_*")
	if err != nil || len(got) != 2 {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 2, 2, len(got))
	}
}

func TestClient_Hash(t *testing.T) {
	key := "qwe_key"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	aa := 123
	err := c.HSet(logPrefix, key, "field1", aa)
	if err != nil {
		t.Error(err.Error())
		return
	}
	bb := "test"
	err = c.HSet(logPrefix, key, "field2", bb)
	if err != nil {
		t.Error(err.Error())
		return
	}
	c.Expire(logPrefix, key, time.Second)
	got, err := c.HGetString(logPrefix, key, "field2")
	if err != nil || got != bb {
		t.Error("Ошибка HGet")
	}
	
	gotMap, err := c.HGetAll(logPrefix, key)
	if err != nil || len(gotMap) != 2 {
		t.Error("Ошибка HGetAll")
	}
}

func TestClient_DecrN(t *testing.T) {
	key := "qwe"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	err := c.Set(logPrefix, key, 22, time.Second)
	if err != nil {
		t.Error(err.Error())
		return
	}
	
	got, err := c.DecrN(logPrefix, key, 2)
	if err != nil || got != int64(20) {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 1, 20, got)
	}
	
	got, err = c.DecrN(logPrefix, key, 2)
	if err != nil || got != int64(18) {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 2, 18, got)
	}
	
	got, err = c.GetInt64(logPrefix, key)
	if err != nil || got != int64(18) {
		t.Errorf("Шаг %d:  Ждали %d, получили %d", 3, 18, got)
	}
	
}

func TestPing(t *testing.T) {
	key := "qwe"
	begin()
	
	logPrefix := ee.NewPrefix()
	dCh := make(chan struct{})
	
	c := Init(logPrefix, cfg)
	
	c.Set(logPrefix, key, 1, time.Second)
	c.GetInt(logPrefix, key)
	
	c.StartHealthChecker(time.Second, dCh)
	c.GetInt(logPrefix, key)
	
	time.Sleep(time.Second)
	
}

func TestGetInt(t *testing.T) {
	key := "qwe"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	c.Set(logPrefix, key, 1, time.Second)
	
	val, err := c.GetInt(logPrefix, key)
	if err != nil {
		t.Error(err)
		return
	}
	if val != 1 {
		t.Error("val != 1")
		return
	}
	
	_, err = c.GetInt(logPrefix, key+"--"+key)
	if err == nil {
		t.Error(err)
		return
	}
	if err != ErrNil {
		t.Error("err != ErrNil")
	}
	
}
func TestGetInt64(t *testing.T) {
	key := "qweInt64"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	c.Set(logPrefix, key, 1, time.Second)
	
	val, err := c.GetInt64(logPrefix, key)
	if err != nil {
		t.Error(err)
		return
	}
	if val != 1 {
		t.Error("val != 1")
		return
	}
	
	_, err = c.GetInt64(logPrefix, key+"--"+key)
	if err == nil {
		t.Error(err)
		return
	}
	if err != ErrNil {
		t.Error("err != ErrNil")
	}
	
}

func TestSAddSmem(t *testing.T) {
	key := "qwe"
	a1 := []interface{}{"one", "two", "четыре"}
	a2 := make(map[string]int)
	begin()
	
	for _, a := range a1 {
		a2[a.(string)] = 0
	}
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	if err := c.SAdd(logPrefix, key, a1...); err != nil {
		t.Error("Error#1::", err.Error())
		return
	}
	
	c.Expire(logPrefix, key, 60*time.Second)
	
	got, err := c.SMembers(logPrefix, key)
	if err != nil {
		t.Error("Error#2::", err.Error())
		return
	}
	
	for _, g := range got {
		a2[g]++
	}
	
	for k, v := range a2 {
		if v != 1 {
			t.Error("Error#3 для " + k)
		}
	}
	
}

func TestClient_HashExpire(t *testing.T) {
	key := "qwe_key"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	expiration := time.Duration(5 * time.Second)
	
	aa := 123
	err := c.HSetExpire(logPrefix, key, "field1", aa, expiration)
	if err != nil {
		t.Error(err.Error())
		return
	}
	bb := "test"
	err = c.HSet(logPrefix, key, "field2", bb)
	if err != nil {
		t.Error(err.Error())
		return
	}
	
	got, err := c.HGetString(logPrefix, key, "field2")
	if err != nil || got != bb {
		t.Error("Ошибка HGet")
	}
	time.Sleep(expiration)
	
	gotmap, err := c.HGetAll(logPrefix, key)
	if len(gotmap) != 0 {
		t.Error("Ошибка HGetAll, не сработал expiration")
	}
}

func TestBoolHMSet(t *testing.T) {
	
	key := "qwe_key"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	expiration := time.Duration(5 * time.Second)
	
	m := make(map[string]interface{})
	m["boolT"] = true
	m["boolF"] = false
	m["int"] = 22
	m["string"] = "три"
	m["float"] = 22.22
	
	c.HMSet(logPrefix, key, m)
	c.Expire(logPrefix, key, expiration)
	
	g, _ := c.HGetAll(logPrefix, key)
	for k, v := range g {
		fmt.Printf("k=!%s!,v=!%s!\n", k, v)
		want := fmt.Sprintf("%v", m[k])
		if v != want {
			t.Errorf("Error on %s. Want=%s, got=%s\n", k, want, v)
		}
	}
	
}

func TestBoolHSet(t *testing.T) {
	key := "TestBoolHSet"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	c.HSet(logPrefix, key, "TT", true)
	c.HSet(logPrefix, key, "FF", false)
	c.HSet(logPrefix, key, "SS", "false")
	
	gotT, _ := c.HGetString(logPrefix, key, "TT")
	fmt.Printf("gotT=%#v\n", gotT)
	if fmt.Sprintf("%v", gotT) != "1" {
		t.Errorf("Error on TT: want=\"true\", gotT=%s\n", fmt.Sprintf("%v", gotT))
	}
	gotF, _ := c.HGetString(logPrefix, key, "FF")
	fmt.Printf("gotF=%#v\n", gotF)
	if fmt.Sprintf("%v", gotF) != "0" {
		t.Errorf("Error on FF: want=\"false\", gotF=%s\n", fmt.Sprintf("%v", gotF))
	}
	gotS, _ := c.HGetString(logPrefix, key, "SS")
	fmt.Printf("gotS=%#v\n", gotS)
	if fmt.Sprintf("%v", gotS) != "false" {
		t.Errorf("Error on SS: want=\"false\", gotS=%s\n", fmt.Sprintf("%v", gotS))
	}
	
	gotTT, _ := c.HGet(logPrefix, key, "TT")
	fmt.Printf("!!!!!!!!!!!!!!! gotTT=%#v\n", gotTT)
	
	c.Expire(logPrefix, key, time.Second)
}

func TestHGetAll(t *testing.T) {
	key := "TestBoolHSet2-1"
	key2 := "TestBoolHSet2-2"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	c.HSet(logPrefix, key, "TT", true)
	c.HSet(logPrefix, key, "FF", false)
	c.HSet(logPrefix, key, "SS", "false")
	
	got1, err := c.HGetAll(logPrefix, key)
	fmt.Printf("1::got=%#v, err=%#v\n", got1, err)
	got2, err := c.HGetAll(logPrefix, key2)
	fmt.Printf("2::got=%#v, err=%#v\n", got2, err)
	if got2 == nil {
		fmt.Printf("\t\t2::NIL\n")
	}
	
	time.Sleep(10 * time.Second)
	
	got3, err := c.HGetAll(logPrefix, key)
	fmt.Printf("3::got=%#v, err=%#v\n", got3, err)
	if got3 == nil {
		fmt.Printf("\t\t3::NIL\n")
	}
	got4, err := c.HGetAll(logPrefix, key2)
	fmt.Printf("4::got=%#v, err=%#v\n", got4, err)
	if got4 == nil {
		fmt.Printf("\t\t4::NIL\n")
	}
	
}

func TestExists(t *testing.T) {
	key := "TestExists"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	c.Set(logPrefix, key, 1, time.Second)
	ok, err := c.Exists(logPrefix, key)
	if err != nil {
		t.Error(err.Error())
		return
	}
	if !ok {
		t.Error("Ошибка #1")
	}
	
	c.Del(logPrefix, key)
	ok, err = c.Exists(logPrefix, key)
	if err != nil {
		t.Error(err.Error())
		return
	}
	if ok {
		t.Error("Ошибка #2")
	}
}

func TestWrongCreate(t *testing.T) {
	key := "TestWrongCreate"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	c.Del(logPrefix, key)
	
	val, err := c.GetInt64(logPrefix, key)
	fmt.Printf("val=%#v, err=%v\n", val, err)
	
	val, err = c.GetInt64(logPrefix, key)
	fmt.Printf("val=%#v, err=%v\n", val, err)
	
}

func TestHSetRewrite(t *testing.T) {
	key := "TestWrongCreate"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	
	m := make(map[string]interface{})
	m["one"] = "ONE"
	m["two"] = "TWO"
	
	c.HMSet(logPrefix, key, m)
	
	mm := make(map[string]interface{})
	mm["good"] = "GOOD"
	mm["bad"] = "BAD"
	
	c.HMSet(logPrefix, key, mm)
	
	q, err := c.HGetAll(logPrefix, key)
	if err != nil {
		t.Error(err)
	}
	
	for _, k := range []string{"one", "two", "good", "bad"} {
		if _, ok := q[k]; !ok {
			t.Error("нет k=" + k)
		}
	}
	
	c.Del(logPrefix, key)
	
}

func TestClient_DecrNToLim(t *testing.T) {
	key := "TestDecrNToLim"
	begin()
	
	logPrefix := ee.NewPrefix()
	
	c := Init(logPrefix, cfg)
	c.Del(logPrefix, key)
	c.Del(logPrefix, key+"!!")
	
	type args struct {
		logPrefix ee.LogPrefixType
		key       string
		n         int
		lim       int
	}
	tests := []struct {
		name    string
		args    args
		want    int64
		want1   bool
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				logPrefix: logPrefix,
				key:       key,
				n:         5,
				lim:       0,
			},
			want:    5,
			want1:   false,
			wantErr: false,
		},
		{
			name: "n > 10",
			args: args{
				logPrefix: logPrefix,
				key:       key,
				n:         15,
				lim:       0,
			},
			want:    0,
			want1:   true,
			wantErr: false,
		},
		{
			name: "to 5",
			args: args{
				logPrefix: logPrefix,
				key:       key,
				n:         7,
				lim:       5,
			},
			want:    5,
			want1:   true,
			wantErr: false,
		},
		{
			name: "ErrNil",
			args: args{
				logPrefix: logPrefix,
				key:       key + "!!",
				n:         15,
				lim:       0,
			},
			want:    0,
			want1:   false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c.Set(tt.args.logPrefix, key, 10, time.Second)
			got, got1, err := c.DecrNToLim(tt.args.logPrefix, tt.args.key, tt.args.n, tt.args.lim)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecrNToLim() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecrNToLim() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("DecrNToLim() got1 = %v, want1 %v", got1, tt.want1)
			}
		})
	}
}

func TestClient_GetSet(t *testing.T) {
	key := "qwe"
	beginWLevel(3)
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	c.Del(logPrefix, key)
	
	rez, err := c.GetSet(logPrefix, key, key, time.Second)
	fmt.Printf("1::rez=%v\n1::err=%v\n\n", rez, err)
	if len(rez) != 0 || err != ErrNil {
		t.Error("error #1")
	}
	
	rez, err = c.GetSet(logPrefix, key, key, time.Second)
	fmt.Printf("2::rez=%v\n2::err=%v\n\n", rez, err)
	if len(rez) == 0 && (err != nil) {
		t.Error("error #2")
	}
	
	rez, err = c.GetSet(logPrefix, key, key, time.Second)
	fmt.Printf("3::rez=%v\n3::err=%v\n\n", rez, err)
	if len(rez) == 0 && (err != nil) {
		t.Error("error #3")
	}
	
	c.Del(logPrefix, key)
	
	rez, err = c.GetSet(logPrefix, key, key, time.Second)
	fmt.Printf("4::rez=%v\n4::err=%v\n\n", rez, err)
	if len(rez) != 0 || err != ErrNil {
		t.Error("error #4")
	}
}
