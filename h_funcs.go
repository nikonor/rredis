package rredis

import (
	"fmt"
	"strconv"
	"time"
	
	"github.com/go-redis/redis"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

// HSet - установка значения хеш-таблицы
func (c *Client) HSet(logPrefix ee.LogPrefixType, key, field string, value interface{}) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HSet. Ключ=" + key +
		", поле=" + field +
		", значение=" + fmt.Sprintf("%v", value)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HSet."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	return c.cli.HSet(key, field, value).Err()
}

// HSetExpire - установка значения хеш-таблицы с временем истечения ключа
func (c *Client) HSetExpire(logPrefix ee.LogPrefixType, key, field string, value interface{}, expiration time.Duration) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HSetExpire. Ключ=" + key +
		", поле=" + field +
		", значение=" + fmt.Sprintf("%v", value) +
		", expiration=" + fmt.Sprintf("%s", expiration)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HSetExpire."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	if err := c.cli.HSet(key, field, value).Err(); err != nil {
		return err
	}
	return c.cli.Expire(key, expiration).Err()
}

// HMSet - установка хеш-таблицы
func (c *Client) HMSet(logPrefix ee.LogPrefixType, key string, fields map[string]interface{}) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HMSet. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HMSet."))
	}()
	for k, v := range fields {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HMSet хочет установить: " + k + "=>" + fmt.Sprintf("%v", v)))
	}
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	return c.cli.HMSet(key, fields).Err()
}

// HMSetExpire - установка хеш-таблицы с временем истечения ключа
func (c *Client) HMSetExpire(logPrefix ee.LogPrefixType, key string, fields map[string]interface{}, expiration time.Duration) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HMSetExpire. Ключ=" + key))
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HMSetExpire. Expiration=" + fmt.Sprintf("%s", expiration)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HMSetExpire."))
	}()
	for k, v := range fields {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HMSetExpire хочет установить: " + k + "=>" + fmt.Sprintf("%v", v)))
	}
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	if err := c.cli.HMSet(key, fields).Err(); err != nil {
		return err
	}
	return c.cli.Expire(key, expiration).Err()
}

// HGet - получение значения хэш-таблицы
func (c *Client) HGet(logPrefix ee.LogPrefixType, key, field string) (interface{}, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HGet. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HGet."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	var res interface{}
	
	err := c.cli.HGet(key, field).Scan(&res)
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Возврат empRedis.HGet:" + fmt.Sprintf("%v", res)))
	if err == redis.Nil {
		return res, ErrNil
	} else if err != nil {
		return res, err
	}
	
	return res, err
}

// HGetInt64 получаем значение int64
func (c *Client) HGetInt64(logPrefix ee.LogPrefixType, key, field string) (int64, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HGetInt64. Ключ=" + key +
		", поле=" + field))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HGetInt64."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	res, err := c.cli.HGet(key, field).Int64()
	if err == redis.Nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetInt64 вернул Nil"))
		return res, ErrNil
	} else if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetInt64 вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetInt64 вернул=" + strconv.FormatInt(res, 10)))
	
	return res, nil
}

// HGetString - получаем значение string
func (c *Client) HGetString(logPrefix ee.LogPrefixType, key, field string) (string, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HGetString. Ключ=" + key +
		", поле=" + field))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HGetString."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	res, err := c.cli.HGet(key, field).Result()
	if err == redis.Nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetString вернул Nil"))
		return res, ErrNil
	} else if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetString вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HGetString вернул=" + res))
	
	return res, nil
}

// HGetAll - получаем  все значения хеш-таблицы
func (c *Client) HGetAll(logPrefix ee.LogPrefixType, key string) (map[string]string, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HGetAll. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HGetAll."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	res, err := c.cli.HGetAll(key).Result()
	for k, v := range res {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("  Вызов empRedis.HGetAll вернул: " + k + "=>" + v))
	}
	if err == redis.Nil {
		return res, ErrNil
	} else if err != nil {
		return res, err
	}
	
	return res, err
}

// HDel - удалить значения из хэш таблицы
func (c *Client) HDel(logPrefix ee.LogPrefixType, key string, fields ...string) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HDel Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HIncr."))
	}()
	for _, f := range fields {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HDel хочет удалить поле: " + f))
	}
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	return c.cli.HDel(key, fields...).Err()
}

// HIncr получаем значение int64
func (c *Client) HIncr(logPrefix ee.LogPrefixType, key, field string, diff int64) (int64, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HIncr. Ключ=" + key +
		", поле=" + field))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HIncr."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	res, err := c.cli.HIncrBy(key, field, diff).Result()
	if err == redis.Nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HIncr вернул Nil"))
		return res, ErrNil
	} else if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HIncr вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.HIncr вернул=" + strconv.FormatInt(res, 10)))
	
	return res, nil
}

func (c *Client) HKeys(logPrefix ee.LogPrefixType, key string) ([]string, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.HKeys. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.HKeys."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	rez, err := c.cli.HKeys(key).Result()
	if (err != nil && err == redis.Nil) ||
		(err == nil && (rez == nil || len(rez) == 0)) {
		return rez, ErrNil
	}
	
	return rez, err
}
