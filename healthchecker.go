package rredis

import (
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

func (c *Client) StartHealthChecker(dur time.Duration, doneChan <-chan struct{}) {
	c.Lock()
	defer c.Unlock()
	logPrefix := ee.NewPrefix()
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Старт StartHealthChecker"))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание StartHealthChecker"))
	}()
	
	if c.healthCheckerStarted {
		ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("HealthChecker уже запущен"))
		return
	}
	
	go func(logPrefix ee.LogPrefixType, c *Client, dur time.Duration) {
		ticker := time.NewTicker(dur)
		for {
			select {
			case <-ticker.C:
				if err := c.Ping(logPrefix); err != nil {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithError(err).WithMessage("StartHealthChecker::ошибка при Ping"))
				} else {
					if c.debug {
						ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("StartHealthChecker::Ok"))
					}
				}
			case <-c.doneChan:
				ll.Log(ee.Debug().WithMessage("StartHealthChecker::Из dbWrap получена команда на выход. Выходим.").WithPrefix(logPrefix))
				c.Lock()
				c.healthCheckerStarted = false
				c.Unlock()
				
				return
			case <-doneChan:
				ll.Log(ee.Debug().WithMessage("StartHealthChecker::Из приложения получена команда на выход. Выходим.").WithPrefix(logPrefix))
				c.Lock()
				c.healthCheckerStarted = false
				c.Unlock()
				
				return
			}
		}
	}(logPrefix, c, dur)
	
	c.healthCheckerStarted = true
}
