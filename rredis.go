package rredis

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"
	
	"github.com/go-redis/redis"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

type CfgRedis struct {
	Host     string `yaml:"host" json:"host"`
	Port     int    `yaml:"port" json:"port"`
	Password string `yaml:"password" json:"password"`
	DB       int    `yaml:"db" json:"db"`
}

const (
	// ErrNil - переопределение ошибки redis.Nil
	ErrNil = redis.Nil
)

// Client - клиент для Redis
type Client struct {
	cli *redis.Client
	sync.Mutex
	healthCheckerStarted bool
	doneChan             chan struct{}
	debug                bool
}

func (c *Client) DebugToggle() {
	c.Lock()
	defer c.Unlock()
	c.debug = !c.debug
}

// Init - создаем клиента
func Init(logPrefix ee.LogPrefixType, cfg CfgRedis) *Client {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Создаем клиента для доступа к redis. Сервер=" + cfg.Host + ":" + strconv.Itoa(cfg.Port)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание создания клиента redis"))
	}()
	cli := redis.NewClient(&redis.Options{
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Password: cfg.Password,
		DB:       cfg.DB,
	})
	return &Client{
		cli:      cli,
		doneChan: make(chan struct{}),
	}
}

// Exists - проверка существования ключа
func (c *Client) Exists(logPrefix ee.LogPrefixType, key string) (bool, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Exists. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Exists."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	v, err := c.cli.Exists(key).Result()
	if err != nil {
		if err == redis.Nil {
			return false, nil
		}
		return false, err
	}
	if v == 1 {
		return true, nil
	}
	return false, nil
}

// Set - устанавливаем значение
func (c *Client) Set(logPrefix ee.LogPrefixType, key string, value interface{}, expiration time.Duration) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Set. Ключ=" + key + ", значение=" + fmt.Sprintf("%v", value) + ", ttl=" + expiration.String()))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Set."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	return c.cli.Set(key, value, expiration).Err()
}

// GetInt - получаем значение Int
func (c *Client) GetInt(logPrefix ee.LogPrefixType, key string) (int, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetInt. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetInt."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	res, err := cmd.Int()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt вернул=" + strconv.Itoa(res)))
	
	return res, nil
}

// GetInt64 - получаем значение Int64
func (c *Client) GetInt64(logPrefix ee.LogPrefixType, key string) (int64, error) {
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetInt64. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetInt64."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt64 вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	res, err := cmd.Int64()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt64 вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetInt64 вернул=" + strconv.FormatInt(res, 10)))
	
	return res, nil
}

// GetFloat64 - получаем значение Int64
func (c *Client) GetFloat64(logPrefix ee.LogPrefixType, key string) (float64, error) {
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetFloat64. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetFloat64."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetFloat64 вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	res, err := cmd.Float64()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetFloat64 вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetFloat64 вернул=" + strconv.FormatFloat(res, 'f', 4, 32)))
	
	return res, nil
}

// GetString - получаем значение string
func (c *Client) GetString(logPrefix ee.LogPrefixType, key string) (string, error) {
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetString. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetString."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetString вернул Nil"))
			return "", ErrNil
		}
		return "", err
	}
	
	res, err := cmd.Result()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetString вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetString вернул=" + res))
	
	return res, nil
}

// GetBytes - получаем значение []byte
func (c *Client) GetBytes(logPrefix ee.LogPrefixType, key string) ([]byte, error) {
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetBytes. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetBytes."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetBytes вернул Nil"))
			return nil, ErrNil
		}
	}
	res, err := cmd.Bytes()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetBytes вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetBytes вернул=" + string(res)))
	
	return res, nil
}

// Get - получаем значение interface{}
func (c *Client) Get(logPrefix ee.LogPrefixType, key string) (interface{}, error) {
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	var res interface{}
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Get. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Get."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Get(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Get вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	err := cmd.Scan(&res)
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Get вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Get вернул=" + fmt.Sprintf("%v", res)))
	
	return res, err
}

// Incr - увеличиваем счетчик на 1
func (c *Client) Incr(logPrefix ee.LogPrefixType, key string) (int64, error) {
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Incr. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Incr."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Incr(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Incr вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	res, err := cmd.Result()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Incr вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Incr вернул=" + strconv.FormatInt(res, 10)))
	
	return res, err
}

// IncrN - увеличиваем счетчик на N
func (c *Client) IncrN(logPrefix ee.LogPrefixType, key string, n int) (int64, error) {
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.IncrN. Ключ=" + key + ". Нужно добавить " + strconv.Itoa(n)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.IncrN."))
	}()
	
	for i := 0; i < n-1; i++ {
		cmd := c.cli.Incr(key)
		if err := cmd.Err(); err != nil {
			if err == redis.Nil {
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.IncrN вернул Nil"))
				return 0, ErrNil
			}
			return 0, err
		}
	}
	res, err := c.cli.Incr(key).Result()
	
	if err == redis.Nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.IncrN вернул Nil"))
		return res, ErrNil
	} else if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.IncrN вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.IncrN вернул=" + strconv.FormatInt(res, 10)))
	
	return res, err
}

// Decr - уменьшаем счетчик на 1
func (c *Client) Decr(logPrefix ee.LogPrefixType, key string) (int64, error) {
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Decr. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Decr."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.Decr(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Decr вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	res, err := cmd.Result()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Decr вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.Decr вернул=" + strconv.FormatInt(res, 10)))
	
	return res, err
}

// DecrN - уменьшаем счетчик на N
func (c *Client) DecrN(logPrefix ee.LogPrefixType, key string, n int) (int64, error) {
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.DecrN. Ключ=" + key + ". Нужно уменьшить на " + strconv.Itoa(n)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.DecrN."))
	}()
	
	for i := 0; i < n-1; i++ {
		cmd := c.cli.Decr(key)
		if err := cmd.Err(); err != nil {
			if err == redis.Nil {
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrN вернул Nil"))
				return 0, ErrNil
			}
			return 0, err
		}
	}
	
	cmd := c.cli.Decr(key)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrN вернул Nil"))
			return 0, ErrNil
		}
		return 0, err
	}
	
	res, err := cmd.Result()
	if err == redis.Nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrN вернул Nil"))
		return res, ErrNil
	} else if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrN вернул ошибку").WithError(err))
		return res, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrN вернул=" + strconv.FormatInt(res, 10)))
	
	return res, err
}

// Del - удалить ключ или несколько
func (c *Client) Del(logPrefix ee.LogPrefixType, keys ...string) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Del. Ключ=", keys))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Del."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	return c.cli.Del(keys...).Err()
}

// Rename - переименовать ключ
func (c *Client) Rename(logPrefix ee.LogPrefixType, oldKey, newKey string) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Rename. Ключ=" + oldKey + " в " + newKey))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Rename."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	return c.cli.Rename(oldKey, newKey).Err()
	
}

// Expire - утановить время жизни ключа
func (c *Client) Expire(logPrefix ee.LogPrefixType, key string, expiration time.Duration) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Expire. Ключ=" + key + " ,ttl=" + expiration.String()))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Expire."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	return c.cli.Expire(key, expiration).Err()
}

// Keys - возвращает список ключей по маске
func (c *Client) Keys(logPrefix ee.LogPrefixType, pattern string) ([]string, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Keys. Ключ=" + pattern))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Keys."))
	}()
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	res, err := c.cli.Keys(pattern).Result()
	for _, s := range res {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Keys нашел ключ" + s))
	}
	return res, err
}

// Ping - пинговать Redis
func (c *Client) Ping(logPrefix ee.LogPrefixType) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Ping"))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.Ping"))
	}()
	err := c.cli.Ping().Err()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.Ping получил ошибку.").WithError(err))
	}
	return err
}

// TTL - получить ttl ключа
func (c *Client) TTL(logPrefix ee.LogPrefixType, key string) (time.Duration, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.TTL"))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.TTL"))
	}()
	t, err := c.cli.TTL(key).Result()
	if err != nil {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.TTL полчил ошибку.").WithError(err))
	}
	return t, err
}

func (c *Client) SAdd(logPrefix ee.LogPrefixType, key string, values ...interface{}) error {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.SAdd"))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.SAdd"))
	}()
	if len(values) > 0 {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Выставляем для ключа "+key, values))
		if _, err := c.cli.SAdd(key, values...).Result(); err != nil {
			return err
		}
	}
	return nil
}

func (c *Client) SMembers(logPrefix ee.LogPrefixType, key string) ([]string, error) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.SMembers"))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.SMembers"))
	}()
	ret, err := c.cli.SMembers(key).Result()
	if err != nil {
		return nil, err
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).
		WithMessage("Получили массив по ключу " + key + ":" + strings.Join(ret, ",")))
	
	return ret, nil
}

// DecrNToLim - уменьшаем счетчик на N, но не меньше 0
func (c *Client) DecrNToLim(logPrefix ee.LogPrefixType, key string, n, lim int) (int64, bool, error) {
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.DecrNToLim. Ключ=" + key + ". Нужно уменьшить на " + strconv.Itoa(n)))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.DecrNToLim."))
	}()
	
	count, err := c.cli.Exists(key).Result()
	if err != nil || count == 0 {
		if err == redis.Nil || count == 0 {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.DecrNToLim вернул Nil"))
			return 0, false, ErrNil
		}
		return 0, false, err
	}
	
	var res int64
	
	for i := 0; i < n; i++ {
		res, err = c.cli.Decr(key).Result()
		if err != nil {
			return 0, false, err
		}
		println("i=", i, ", res=", res)
		if res == int64(lim) {
			return res, true, nil
		}
	}
	
	return res, false, nil
}

// GetSet - получаем значение string
func (c *Client) GetSet(logPrefix ee.LogPrefixType, key string, val string, expiration time.Duration) (string, error) {
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов empRedis.GetSet. Ключ=" + key))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание empRedis.GetSet."))
	}()
	
	if !c.healthCheckerStarted {
		c.Ping(logPrefix)
	}
	
	cmd := c.cli.GetSet(key, val)
	c.cli.Expire(key, expiration)
	if err := cmd.Err(); err != nil {
		if err == redis.Nil {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("    empRedis.GetSet вернул Nil"))
			return "", ErrNil
		}
		return "", err
	}
	
	return cmd.String(), nil
}

func (c *Client) GetCli() *redis.Client {
	return c.cli
}
