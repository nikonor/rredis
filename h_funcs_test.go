package rredis

import (
	"sort"
	"testing"
	
	ee "gitlab.com/nikonor/eerrors"
)

func TestClient_HIncr(t *testing.T) {
	key := "tstKey"
	field := "BB"
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	c.HSet(logPrefix, key, "AA", "one")
	c.HSet(logPrefix, key, field, 22)
	
	type args struct {
		key   string
		field string
		diff  int64
	}
	tests := []struct {
		name    string
		args    args
		want    int64
		wantErr bool
	}{
		{
			name: "Add 1 => 23",
			args: args{
				key:   key,
				field: field,
				diff:  1,
			},
			want:    23,
			wantErr: false,
		},
		{
			name: "Add -1 => 22",
			args: args{
				key:   key,
				field: field,
				diff:  -1,
			},
			want:    22,
			wantErr: false,
		},
		{
			name: "Add 0 => 22",
			args: args{
				key:   key,
				field: field,
				diff:  0,
			},
			want:    22,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := c.HIncr(logPrefix, tt.args.key, tt.args.field, tt.args.diff)
			if (err != nil) != tt.wantErr {
				t.Errorf("HIncr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("HIncr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHKeys(t *testing.T) {
	key := "TestHKeys"
	data := map[string]interface{}{
		"aaa": 1,
		"bbb": 2,
		"ccc": 3,
	}
	begin()
	logPrefix := ee.NewPrefix()
	c := Init(logPrefix, cfg)
	
	c.HMSet(logPrefix, key, data)
	
	t1, err := c.HKeys(logPrefix, key+key)
	if err == nil || err != ErrNil {
		t.Errorf("Error#1:t1=%#v,err=%#v\n", t1, err)
	}
	
	t1, err = c.HKeys(logPrefix, key)
	if t1 == nil || len(t1) != 3 || err != nil {
		t.Error("Error#2")
	}
	
	sort.Strings(t1)
	if t1[0] != "aaa" {
		t.Error("Error #3.0")
	}
	if t1[1] != "bbb" {
		t.Error("Error #3.1")
	}
	if t1[2] != "ccc" {
		t.Error("Error #3.2")
	}
	
}
